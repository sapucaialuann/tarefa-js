/*
São duas tarefas:
    1 - botão de excluir remover toda a tag que o contém
    2 - botão de adicionar insere na lista
*/


//este bloco remove a atualização automática do botão "submit"
const buttonForm = document.querySelector("#add-book");
buttonForm.addEventListener("click", (e) => {
    e.preventDefault();
});

//este bloco cria um novo elemento na lista
let myElement = document.querySelector('#add-book');
myElement.addEventListener("click", (e) => {
    let input = document.querySelector("#text");
    let lista = document.querySelector('#lista-toda');
    let botaoRemove = document.querySelector('.delete');
    let elemento = document.createElement('li');
    let textoElemento = document.createTextNode(input.value);
    if(e.target.classList == 'botao-lista'){
        elemento.appendChild(textoElemento);
        elemento.appendChild(botaoRemove);
        lista.appendChild(elemento);
    };
});

//este bloco remove o elemento da lista
let botaoExcluir = document.querySelector('ul');
botaoExcluir = document.addEventListener('click', (e) => {
    e.preventDefault();
    if (e.target.classList == 'delete') {
        e.target.parentNode.remove();
    }
});